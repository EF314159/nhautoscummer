NetHack AutoScummer
===================

    Java -jar NHAutoScummer.jar [--verbose]
Look in [Downloads](https://bitbucket.org/EF314159/nhautoscummer/downloads/) for the latest version! (1.0.0)

NHAutoScummer is a Java tool to quickly join and #quit NetHack games on public DGameLaunch servers until the starting inventory and/or stats meets the user-defined criteria. Given a settings file with info on a public server, it can log in, navigate to the game/variant's menu, then quickly start, #quit, and restart. This process is highly configurable in the settings menu to account for different DGamelaunch menus.

    srvName = "nethack"             // server name ('nethack' in 'nethack@alt.org')
    srvHost = "alt.org"             // hostname 'alt.org' in 'nethack@alt.org')
    srvPassword = "nethack"         // server password (NOT your account password!)
    
    loginKey = "l"                  // key or input sequence to get to dgamelaunch's login prompt
    
    accountName = ""                // your account name
    accountPassword = "hunter2"     // your account password
    
    pathToGameMenu = "p"            // 'p'lay Nethack 3.6.1 (opens NAO 3.6.1 menu)
    
    initGame = "p"                  // 'p'lay Nethack 3.6.1 (starts the game from 3.6.1 menu)
    character = "nwem"              // 'n'o, 'w'izard, 'e'lf, 'm'ale

Game criteria are defined in the settings file as regexes. Each regex is matched against a printout of the character's inventory and status bar, and the game is quit unless every one can be found:

    regex = "magic marker \(0:[6-9][0-9]\)"
    regex = "ring of slow digestion"
    regex = "wand of wishing"

(Don't actually do this. You can't start with a wand of wishing.)

You can get more debug output from the command line with the --verbose option. NHAutoScummer has been tested with both Nethack.alt.org and Hardfought, but report any bugs I haven't found to me (ShivanHunter on #Hardfought or anywhere else or on here).

Good luck with TNNT everyone!
