
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class NHAutoScummer {
	private OutputStream fout;
	private InputStream fin;
	
	private static String srvName = "nethack"; // server name ('nethack' in 'nethack@alt.org')
	private static String srvHost = "alt.org"; // hostname 'alt.org' in 'nethack@alt.org')
	private static String srvPassword = "nethack"; // server password (NOT your account password!)
	
	private static String loginKey = "l"; // key or input sequence to get to dgamelaunch's login prompt
	
	private static String accountName = "";
	private static String accountPassword = "";
	
	private static String pathToGameMenu = "p"; // 'p'lay Nethack 3.6.1 (opens NAO menu)
	
	private static String initGame = "p"; // 'p'lay Nethack 3.6.1
	private static String character = "nwem"; //'n'o, 'w'izard, 'e'lf, 'm'ale
	private static String logout = "qq"; // 'q'uit from 361 menu, then 'q'uit from dgame menu
	private static String save = "Sy"; // 'q'uit from 361 menu, then 'q'uit from dgame menu
	private static String quit = "#quit\\ry"; // 'q'uit from 361 menu, then 'q'uit from dgame menu
	
	private static List<String> regex = new LinkedList<String>();
	
	private static int bailout = 500; // max number of games to try
	private static int waitTime = 100; // number of ms to wait to make sure entire response is received
	
	private static boolean verbose = false;

	public static void main(String args[]) throws IOException {
		NHAutoScummer app = new NHAutoScummer();
		
		for (String arg : args) {
			if (arg.equals("-v") || arg.equals("--verbose")) verbose = true;
		}

		System.out.println("NHAutoScummer v1.0.0");
		if (!verbose) {
			System.out.println("USe -v or --verbose to see everything ssh is doing.");
		}
		
		loadSettings();
		
		if (!accountName.isEmpty() && !accountPassword.isEmpty()) {
			app.initSession();
		} else {
			System.out.println("Blank username or password - did you set them in autoscummer.ini?");
		}
	}
	
	/*
	 * Loads settings from autoscummer.ini, replacing the defaults.
	 */
	private static void loadSettings() throws IOException {
		List<String> lines = Files.readAllLines(Paths.get("autoscummer.ini"));
		
		// read each line and match it to a setting
		for (String line : lines) {
			
			if (line.indexOf("//") != -1) {
				line = line.substring(0, line.indexOf("//")); // crop out comments
			}
			
			String[] tokens = line.split("=");
			
			// ignore blank lines
			if (tokens.length < 1) continue;
			
			switch(tokens[0].trim()) {
				case "srvName":         srvName = parseString(tokens[1]); break;
				case "srvHost":         srvHost = parseString(tokens[1]); break;
				case "srvPassword":     srvPassword = parseString(tokens[1]); break;
				case "loginKey":        loginKey = parseString(tokens[1]); break;
				case "accountName":     accountName = parseString(tokens[1]); break;
				case "accountPassword": accountPassword = parseString(tokens[1]); break;
				case "pathToGameMenu":  pathToGameMenu = parseString(tokens[1]); break;
				case "initGame":        initGame = parseString(tokens[1]); break;
				case "character":       character = parseString(tokens[1]); break;
				case "save":            save = parseString(tokens[1]); break;
				case "quit":            quit = parseString(tokens[1]); break;
				case "logout":          logout = parseString(tokens[1]); break;
				case "bailout":         bailout = Integer.parseInt(tokens[1].trim()); break;
				case "waitTime":        waitTime = Integer.parseInt(tokens[1].trim()); break;
				case "regex":           regex.add(parseString(tokens[1])); break;
			}
		}
	}
	
	/*
	 * Parse a string for the ini file. Returns any string in quotes inside the given string,
	 * or NULL if there is no match. Also resolves escape characters in the line:
	 * "\n" is replaced with an actual newline.
	 */
	private static String parseString(String in) {
		int firstQuote, secondQuote;
		
		firstQuote = in.indexOf('"')+1;
		secondQuote = in.indexOf('"', firstQuote);
		
		if (firstQuote == 0 || secondQuote == -1) return null;
		
		String out = in.substring(firstQuote, secondQuote);
		
		out = out.replace("\\t", "\t");
		out = out.replace("\\b", "\b");
		out = out.replace("\\n", "\n");
		out = out.replace("\\r", "\r");
		out = out.replace("\\f", "\f");
		out = out.replace("\\'", "'");
		out = out.replace("\\\"", "\"");
		out = out.replace("\\\\", "\\");
		
		return out;
	}

	/*
	 * Starts an SSH session using the JSch library. Sets fout and fin as the session's
	 * input and output streams.
	 */
	private void initSession() {
		try {
            JSch jsch=new JSch();
 
            Session session=jsch.getSession(srvName, srvHost, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setTimeout(60000);
            session.setPassword(srvPassword);
            session.connect(60000);
 
            Channel channel=session.openChannel("shell");
			
			channel.connect();
			
			fout = channel.getOutputStream();
			fin = channel.getInputStream();
			
			// send data over the ssh session
			// put() blocks until something gets sent back, and returns the response as a String
			put(loginKey + accountName + "\r" + accountPassword + "\r" + pathToGameMenu);
			
			System.out.println("Attempting to startscum " + bailout + " times for: ");
			for (String r : regex) {
				System.out.println("\t\"" + r + "\"");
			}
			
			// do stuff
			startscum();
			
			// quit entirely
			fout.write((logout).getBytes());
			fout.flush();
			
			session.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * The main loop - tries [bailout] times to get a game which matches all given regexes
	 */
	private void startscum() throws IOException, InterruptedException {
		for (int i = 0; i < bailout; ++i) {
			// navigate to game menu from main dgamelaunch menu
			// have to wait before character creation -
			// dgamelaunch flushes the buffer or something, idk
			put(initGame);
			
			// choose character, start game, space through any initial messages
			put(character + "\r          ");
			
			// one more space refreshes the stat bar, also get inventory
			String readout =  put(" i");
			
			// ESC out of inventory (wiz can sometimes be two pages)
			put("");
			
			if (regex.size() < 1 || check(readout)) {
				// FOUND IT!
				// save and quit
				put(save);
				System.out.println("\nSuccess! Game meeting the requirements has been saved.");
				System.out.println(i + " brave adventurers were harmed in the scumming of this start.");
				return;
			} else {
				// quit back to main menu
				String out = put(quit);
				
				if (!verbose) System.out.print(".");
				
				// Hardfought asks to overwrite ttyrecs if created in the same second
				if (Pattern.compile("do you wish to overwrite").matcher(out).find()) {
					put("y");
				}
			}
		}
		System.out.println("\nNo game found after " + bailout + " tries. Giving up...");
		
		// cmon bro wtf are you doing
		if (bailout > 500) {
			System.out.println(
					  "Keep in mind the combination you are trying to achieve may not be possible. For\n"
					+ "instance, NetHack will prevent wizards from starting with a polymorph item and \n"
					+ "a ring of polymorph control, and you will NEVER start with a wand of wishing!\n"
					+ "Also check your regexes for typos; You can't get a \"ring of slow dgiestion\" no\n"
					+ "matter how hard you try!");
		}
	}
	
	/*
	 * Given a string representing both the inventory and stat bar readouts,
	 * checks it to find() every given regex. Returns true if a match is found.
	 * Always returns true if there are no regexes to match.
	 */
	private boolean check(String readout) {
		for (String r : regex) {
			if (!Pattern.compile(r).matcher(readout).find()) return false;
		}
		return true;
	}
	
	/*
	 * Sends data over the ssh connection.
	 * Also calls ReadAll() to block until we get a response. 
	 */
	private String put(String in) throws IOException, InterruptedException {
		if (verbose) System.out.println("SEND: \"" + in + "\"\n");
		fout.write(in.getBytes());
		fout.flush();
		return readAll();
	}
	
	/*
	 * Reads data coming from the ssh connection. Will block until any data is received,
	 * and will wait for [waitTime] more ms after reading the buffer in case any packets were delayed.
	 */
	private String readAll() throws IOException, InterruptedException {
		StringBuilder out = new StringBuilder();
		
		// prune all the ugly CSI sequences for terminals (cursor position, colors)
		// everything we need to be contiguous (item names, stats) is already contiguous so it's fine
		boolean inCSISequence = false;
		
		// block until we get some data
		while (fin.available() < 1) {
			Thread.sleep(100);
		}
		
		// read until buffer is empty
		while (fin.available() > 0) {
			while (fin.available() > 0) {
				int c = fin.read();
				
				if (c == 27) {
					inCSISequence = true;
					fin.read(); // also get the [ after the ESC
				}
				
				// read the character
				if (!inCSISequence) out.append((char)c);
				
				// CSI sequences always end with a char in 0x40-0x7E
				else if (c >= 0x40 && c < 0x7F) {
					inCSISequence = false;
				}
			}
			// wait a bit after finishing then loop again to make sure we've got the response
			Thread.sleep(waitTime);
		}
		
		if (verbose) System.out.println("READ: " + out.toString() + "\n");
		return out.toString();
	}
}